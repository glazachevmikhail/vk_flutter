import 'package:flutter/material.dart';
import 'package:vk_flutter/ui/widgets/auth_screen/auth_screen_widget.dart';
import 'package:vk_flutter/ui/widgets/frends_screen/frends_screen_widget.dart';
import 'package:vk_flutter/ui/widgets/main_screen/main_screen.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context) => const AuthScreenWidget(),
        '/frends_screen': (context) => const FrendsWidget(),
        '/main_screen': (context) => const MainScreenWidget(),
      },
      initialRoute: '/',
    );
  }
}
