import 'package:flutter/material.dart';
import 'package:vk_flutter/ui/widgets/auth_screen/widgets/auth_login_text_field_widget.dart';
import 'package:vk_flutter/ui/widgets/auth_screen/widgets/auth_password_text_field_widget.dart';
import 'package:vk_flutter/ui/widgets/auth_screen/widgets/button_auth_widget.dart';
import 'package:vk_flutter/ui/widgets/auth_screen/widgets/logo_vk_widget.dart';

class AuthScreenWidget extends StatefulWidget {
  const AuthScreenWidget({super.key});

  @override
  State<AuthScreenWidget> createState() => _AuthScreenWidgetState();
}

class _AuthScreenWidgetState extends State<AuthScreenWidget> {
  final loginController = TextEditingController(text: 'admin');
  final passwordController = TextEditingController(text: 'admin');
  String? errorText = null;

  void auth() {
    if (loginController.text == 'admin' && passwordController.text == 'admin') {
      errorText = null;
      Navigator.of(context).pushReplacementNamed('/main_screen');
    } else if (loginController.text.isEmpty ||
        passwordController.text.isEmpty) {
      errorText = 'поля ввода пусты!';
    } else {
      errorText = 'не верный логин или пароль';
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final errorText = this.errorText;
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(height: 170),
            const LogoVKWidget(),
            const SizedBox(height: 20),
            AuthLoginTextFieldWidget(controller: loginController),
            const SizedBox(height: 10),
            AuthPasswordTextFieldWidget(controller: passwordController),
            const SizedBox(height: 15),
            if (errorText != null)
              Padding(
                padding: const EdgeInsets.only(bottom: 15),
                child: Text(
                  errorText,
                  style: const TextStyle(color: Colors.redAccent),
                ),
              ),
            ButtonAuthWidget(
              radius: 10.0,
              buttonColor: Colors.blue,
              onPressed: auth,
              child: const Text(
                'Войти',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
