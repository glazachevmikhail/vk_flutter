import 'package:flutter/material.dart';

class LogoVKWidget extends StatelessWidget {
  const LogoVKWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        Text('VK', style: TextStyle(fontSize: 40)),
        SizedBox(height: 10),
        Text(
          'Вход ВКонтакте',
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }
}
