import 'package:flutter/material.dart';

class ButtonAuthWidget extends StatelessWidget {
  final VoidCallback onPressed;
  final Widget child;
  final Color buttonColor;
  final double? radius;
  final double? height;
  final double? width;
  final double? splashColor;
  final TextStyle? textStyle;

  const ButtonAuthWidget({
    super.key,
    required this.onPressed,
    required this.child,
    this.buttonColor = Colors.grey,
    this.radius,
    this.splashColor,
    this.height,
    this.width,
    this.textStyle,
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(radius ?? 0),
      child: Material(
        color: buttonColor,
        child: InkWell(
          onTap: onPressed,
          splashColor:
              Colors.red.withOpacity(splashColor ?? 0), // Цвет эффекта Ripple
          borderRadius: BorderRadius.circular(8.0), // Задаем радиус границы
          child: Container(
            height: height,
            width: width,
            padding:
                const EdgeInsets.symmetric(vertical: 12.0, horizontal: 16.0),
            decoration: const BoxDecoration(
              color: Colors.transparent,
            ),
            child: Center(
              child: child,
            ),
          ),
        ),
      ),
    );
  }
}
