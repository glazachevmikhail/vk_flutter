import 'package:flutter/material.dart';
import 'package:vk_flutter/ui/widgets/frends_screen/frends_screen_widget.dart';

class MainScreenWidget extends StatefulWidget {
  const MainScreenWidget({super.key});

  @override
  State<MainScreenWidget> createState() => _MainScreenWidgetState();
}

class _MainScreenWidgetState extends State<MainScreenWidget> {
  int selectedTab = 0;

  void onSelecredTab(int index) {
    if (selectedTab == index) return;
    selectedTab = index;
    setState(() {});
  }

  static const List<Widget> screens = [
    Text(
      'новости',
      style: TextStyle(fontSize: 50),
    ),
    Text(
      'Сообщения',
      style: TextStyle(fontSize: 50),
    ),
    FrendsWidget(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[selectedTab],
      appBar: AppBar(
        title: const Text('ВКонтакте'),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: selectedTab,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.newspaper),
            label: 'новости',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.message),
            label: 'Сообщения',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.people),
            label: 'Друзья',
          ),
        ],
        onTap: (value) => onSelecredTab(value),
      ),
    );
  }
}
